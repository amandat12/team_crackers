import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  pythonForm = new FormGroup({
    python: new FormControl('')
  });

  private _options = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
  closeResult: string;
  result:any;

  constructor(private modalService: NgbModal,
    private http: HttpClient) {}

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    });
  }

  postCode(formData) {
    return this.http.post("http://localhost:3000/api/v1/py", formData, this._options)
  }

  submit() {
    console.log("commit", this.pythonForm.value)
    this.postCode(this.pythonForm.value).subscribe((data: any) => { this.result = data });
    this.pythonForm.reset();
    this.modalService.dismissAll();
  }


  ngOnInit() {
    
  }


}
