var express = require('express');
const fs = require('fs');
const bodyParser = require("body-parser");
var router = express.Router();

router.post('/', function(req, res, next) {
  var py = req.body.python;

  fs.writeFile('/data/script.py', py, (err) => {
    if (err) throw err;
  }); 
  fs.appendFile('/data/script.py', "sys.stdout.flush()", (err) => {
    if (err) throw err;  
  });

  const spawn = require("child_process").spawn;
  const pyprog = spawn('python',["/data/script.py"]); 
  
  pyprog.stdout.on('data', function(results) {
  
    res.json(results.toString('utf8')) 
  });
  pyprog.stderr.on('data', (results) => {
    res.json(results.toString('utf8'))  
  }); 
});
  
module.exports = router;
